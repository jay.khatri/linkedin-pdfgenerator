const tmp = require("temporary");
const fs = require("fs");
const path = require("path");
const exec = require("child_process").exec;

module.exports = (url, buffer) => {
  const geFileExtension = url.split(".").pop();
  return new Promise(function (resolve, reject) {
    const fileName = `./temp.${geFileExtension}`;

    fs.writeFileSync(fileName, buffer);

    const cmd = `unoconv -f pdf --stdout ${fileName}`;

    exec(cmd, function (error, stdout, stderr) {
      if (!error.message.includes('maxBuffer')) {
        reject({
          message: "You cannot share password protected documents on LinkedIn.",
        });
      } else {
        resolve();
      }
      exec(`rm -rf ${fileName}`);
    });
  });
};
