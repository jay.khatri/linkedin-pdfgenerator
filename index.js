require("dotenv").config();
const http = require("http");
const https = require("https");
const fs = require("fs");
const Koa = require("koa");
const bodyParser = require("koa-bodyparser");
const cors = require("@koa/cors");
const helmet = require("koa-helmet");
const responseTime = require("koa-response-time");
const Router = require("koa-router");
const logger = require("koa-logger");
const jwt = require("jsonwebtoken");
const converToPdf = require("./converToPdf");

const app = new Koa();
//  app.use(serve(path.join(__dirname, 'views')))
const router = new Router({});
app.proxy = true;

// Add middlewares
app.use(responseTime());
app.use(logger());
app.use(helmet());
app.use(cors());
app.use(
  bodyParser({
    enableTypes: ["text", "json", "form"],
    onerror(err, ctx) {
      ctx.throw("Invalid Body", 422);
    },
  })
);

const HTTP_PORT = 80;
const HTTPS_PORT = 443;

// When run in local comment certificatePath and https.createServer
const options = {
  key: fs.readFileSync("/etc/ssl/sp/sp_pvt.key", "utf8").toString(),
  cert: fs.readFileSync("/etc/ssl/sp/sp_all.crt", "utf8").toString(),
};

https.createServer(options, app.callback()).listen(HTTPS_PORT, (err) => {
  if (err) {
    console.log("Error occured on starting the server");
    console.log(err);
    return;
  }
  console.log(`HTTPS server run at post: ${HTTPS_PORT}`);
});

http.createServer(app.callback()).listen(HTTP_PORT, (err) => {
  if (err) {
    console.log("Error occured on starting the server");
    console.log(err);
    return;
  }
  console.log(`HTTP server run at post: ${HTTP_PORT}`);
});

router.post("/metadata", async (ctx) => {
  const { authorization } = ctx.headers;

  const token = authorization;
  if (!token || !token.trim()) {
    ctx.status = 400;
    return (ctx.body = {
      status: 400,
      message: "Unauthorised",
    });
  }
  try {
    const decoded = await jwt.verify(
      token.slice(7, token.length),
      process.env.JWT_SECRET_KEY
    );
    if (decoded) {
      const { url } = ctx.request.body;
      if (!url) {
        ctx.status = 400;
        return (ctx.body = {
          status: 400,
          message: "Url Not Found",
        });
      }

      const response = await converToPdf(url, decoded);
      ctx.body = response.error
        ? ctx.throw(response.statusCode, response.message)
        : response;
    } else {
      ctx.status = 400;
      return (ctx.body = {
        status: 400,
        message: "Unauthorised",
      });
    }
  } catch (err) {
    ctx.status = 400;
    ctx.body = {
      status: 400,
      message: err.message || "Oops! something went wrong",
    };
  }
});

router.get("/", async (ctx) => {
  ctx.body = {
    response: "ok",
  };
});

app.use(router.routes()).use(router.allowedMethods());
