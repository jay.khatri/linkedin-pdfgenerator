require("dotenv").config();
const officeToPdf = require("office-to-pdf");
const PDFParser = require("pdf-parse");
const passwordcheck = require("./checkPassword");
const downloadMedia = require("./downloadMedia");
const s3 = require("./awsS3");

module.exports = async (url, token) => {
  try {
    if (!url) {
      ctx.status = 400;
      return (ctx.body = {
        status: 400,
        message: "Document Not Found",
      });
    }
    const getCompanyId = token.companyId;
    if (!getCompanyId) {
      ctx.status = 400;
      return (ctx.body = {
        status: 400,
        message: "Company Not Found",
      });
    }
    const getFileName = url.split("/").slice(-1)[0];
    const documentKey = `${getCompanyId}/${getFileName}`;
    const bucketName = process.env.BUCKET_NAME;

    // download file
    const mediaData = await downloadMedia(documentKey, bucketName);

    // Check File is Password Protected or not
    const checkckPasswordProtected = await passwordcheck(url, mediaData);

    // Convert the file to PDF
    const pdfBuffer = await officeToPdf(mediaData);

    // To get file pages
    const pdfData = await PDFParser(pdfBuffer);

    // Upload the PDF to S3
    const uploadKey = `${documentKey.split(".")[0]}.pdf`;
    await s3
      .upload({
        Bucket: bucketName,
        Key: uploadKey, // add .pdf extension
        Body: pdfBuffer, // upload buffer ata
        ContentType: "application/pdf",
        mime: "application/pdf",
        ContentDisposition: "inline",
        ACL: "public-read",
        Metadata: { ContentType: "application/pdf" },
      })
      .promise();

    return {
      statusCode: 200,
      body: {
        message: "PDF generated successfully",
        url: `https://${bucketName}.s3.amazonaws.com/${uploadKey}`,
        fileSize: pdfBuffer.length,
        numPages: pdfData.numpages,
      },
    };
  } catch (error) {
    return {
      error: true,
      statusCode: 400,
      message: error.message || "PDF generation failed",
    };
  }
};
