const s3 = require("./awsS3");

module.exports = async (url, bucketName) => {
  const errors = {
    invalidMediaType: { message: `Invalid Media type` },
    requestError: { message: `Invalid Request Url` },
    retrievingError: { message: `Error retrieving or saving the doc file` },
  };
  const mediaExtensions = ["pdf", "doc", "docx", "ppt", "pptx"]; // check extensions
  const getExtesnion = url.split(".").pop();
  if (!mediaExtensions.includes(getExtesnion)) {
    throw errors.invalidMediaType;
  }

  const params = {
    Bucket: bucketName,
    Key: url,
  };

  try {
    const response = await s3.getObject(params).promise(); // use s3 function
    const data = response.Body; // get buffer data
    const mediaSize = data.length;
    if (!mediaSize || mediaSize <= 0) {
      throw errors.requestError;
    }
    return data;
  } catch (err) {
    throw errors.retrievingError;
  }
};
